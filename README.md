# InfluxReplicationControl

Script that check if data are replicating on server influxdb instance. If not raspberry pi will be reseting

`chmod +x /opt/influxreplicationcontrol/CheckDataReplication.py
`

`0 * * * * /usr/bin/python3 /opt/influxreplicationcontrol/CheckDataReplication.py`

Create file Credentials.py with structure:

```
influx = {
	'port': xxxx,
	'password': 'xxxxxxxxx',
	'user': 'user',
	'url': 'url',
	'database': 'db_name',
	'item_name': 'item_name',
}

rpi = {
	'hostname': 'host_name'
}
```
