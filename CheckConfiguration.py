from Credentials import influx, rpi
from influxdb import InfluxDBClient
from requests.exceptions import ConnectionError


class InfluxClient:

	def __init__(self, influxUrl, influxPort, dbUName, dbpwd, dbname):
		self.client = InfluxDBClient(influxUrl, influxPort, dbUName, dbpwd, dbname)
		self._checkDataOnServer()

	def _checkDataOnServer(self):
		try:
			result = self.client.query("SELECT \"value\" FROM \"{database}\".\"autogen\".\"{item}\" WHERE time > now() "
			                           "- 1h ORDER BY DESC LIMIT 1"
			                           .format(database=influx['database'], host=rpi['hostname'],
			                                   item=influx['item_name']))
			result = list(result.get_points())
			if len(result) == 0:
				print("No data found in influx server")
			else:
				print("Last data found at {}. Configuration OK".format(result[0]['time']))

		except ConnectionError:
			print("Connection Error")


InfluxClient(influx['url'], influx['port'], influx['user'], influx['password'], influx['database'])
