import logging
import os
from datetime import datetime

from influxdb import InfluxDBClient

from Credentials import influx, rpi

logging.basicConfig(filename='/opt/influxreplicationcontrol/DataReplication.log', filemode='a+',
                    format='%(asctime)s - %(message)s',
                    level=logging.INFO)


class InfluxClient:

	def __init__(self, influxUrl, influxPort, dbUName, dbpwd, dbname):
		self.client = InfluxDBClient(influxUrl, influxPort, dbUName, dbpwd, dbname)
		self._checkDataOnServer()

	def _checkDataOnServer(self):
		try:
			result = self.client.query("SELECT \"value\" FROM \"{database}\".\"autogen\".\"{item}\" WHERE time > now() "
			                           "- 1h ORDER BY DESC LIMIT 1"
			                           .format(database=influx['database'], host=rpi['hostname'],
			                                   item=influx['item_name']))
			result = list(result.get_points())
			if len(result) == 0:
				system_reboot()
			else:
				result_time = datetime.strptime(result[0]['time'].split('.')[0], '%Y-%m-%dT%H:%M:%S')
				diff_in_min = (datetime.utcnow() - result_time).total_seconds() / 60.0
				logging.info('Last data found {} minutes ago'.format(int(diff_in_min) if diff_in_min > 0 else 0))
				if diff_in_min > 30:
					system_reboot()
		except:
			system_reboot()


def system_reboot():
	logging.info('REBOOTING SYSTEM')
	os.system('sudo reboot')


InfluxClient(influx['url'], influx['port'], influx['user'], influx['password'], influx['database'])
